class_name Universe
extends Spatial

onready var simulation : Simulation = $Simulation;

func _ready() -> void:
	self.add_to_group(Groups.UNIVERSE);


func _physics_process(delta: float) -> void:
	$Simulation.tick(10 * delta);
