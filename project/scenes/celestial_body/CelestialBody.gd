tool
class_name CelestialBody
extends Spatial

export var radius : float = 10.0 setget set_radius;
export var surface_gravity : float = 50.0 setget set_surface_gravity;
export var initial_velocity : Vector3 = Vector3.ZERO;
export var surface_color : Color = Color.blue setget set_color;
export var emissive : bool = false setget set_emissive;

var current_velocity : Vector3 = Vector3.ZERO;
var mass : float = 100.0;


func _ready() -> void:
	self.add_to_group(Groups.CELESTIAL_BODY);
	current_velocity = initial_velocity;
	
	generate_planet();


func update_velocity(celestial_bodies : Array, delta : float) -> void:
	for other_body in celestial_bodies:
		if(other_body != self):
#			other_body = other_body as CelestialBody;
			var dst_vec : Vector3 = other_body.transform.origin - self.transform.origin;
			var sqr_dst : float = dst_vec.length_squared();
			var force_dir : Vector3 = dst_vec.normalized();
			var force : Vector3 = force_dir * PhysicsConstants.gravitational_constant\
				* mass * other_body.mass / sqr_dst;
			var acceleration : Vector3 = force / mass;
			current_velocity += acceleration * delta;


func update_position(delta : float) -> void:
	self.transform.origin += current_velocity * delta;


func set_radius(val : float) -> void:
	radius = val;
	if(Engine.editor_hint):
		generate_mesh();


func set_surface_gravity(val : float) -> void:
	surface_gravity = val;
	if(!Engine.editor_hint):
		mass = surface_gravity * radius * radius / PhysicsConstants.gravitational_constant;


func set_color(val : Color) -> void:
	surface_color = val;
	if(Engine.editor_hint):
		generate_material();

func set_emissive(val : bool) -> void:
	emissive = val;
	if(Engine.editor_hint):
		generate_material();


func generate_mesh() -> void:
	var new_mesh : SphereMesh = SphereMesh.new();
	new_mesh.radius = radius;
	new_mesh.height = 2 * radius;
	$MeshInstance.mesh = new_mesh;


func generate_material() -> void:
	var new_mat : SpatialMaterial = SpatialMaterial.new();
	new_mat.albedo_color = surface_color;
	new_mat.emission_enabled = emissive;
	new_mat.emission = surface_color;
	$MeshInstance.material_override = new_mat;


func generate_planet() -> void:
	generate_mesh();
	generate_material();
