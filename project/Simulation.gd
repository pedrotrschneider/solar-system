class_name Simulation
extends Node

var celestial_bodies : Array = [];


func _ready() -> void:
	celestial_bodies = self.get_tree().get_nodes_in_group(Groups.CELESTIAL_BODY);


func tick(delta: float) -> void:
	for celestial_body in celestial_bodies:
#		celestial_body = celestial_body as CelestialBody;
		celestial_body.update_velocity(celestial_bodies, delta);
	
	for celestial_body in celestial_bodies:
#		celestial_body = celestial_body as CelestialBody;
		celestial_body.update_position(delta);
